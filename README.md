# SeekBarCompat
**本项目是基于开源项目SeekBarCompat进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/ahmedrizwan/SeekBarCompat ) 追踪到原项目版本**

#### 项目介绍
 - 项目名称： SeekBarCompat
 - 所属系列：ohos的第三方组件适配移植
 - 功能： SeekBarCompat是一个Slider的封装库
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人： hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/ahmedrizwan/SeekBarCompat#readme
 - 原项目基线版本：v0.2.4   sha1:74c51db9262b2d29a656e070d46bdc3384169124  
 - 编程语言：Java
 - 外部库依赖：

#### 效果展示
<img src="screenshot/SeekBarCompat.gif" />

#### 安装教程
##### 方案一：

1. 下载依赖库har包SeekBarCompat.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方案二：

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'app.minimize.com:seekbar_compat:1.0.0'
 }
```


#### 使用说明
#####  基本使用
SeekBarCompat属性

| Attribute 属性           | Description 描述 |
|:---				       |:---|
| thumbColor               | 拖块颜色            |
| progressColor            | 进度条颜色          |
| progressBackgroundColor  | 进度条背景色     |


### 常见用法：SeekBarCompat

#### XML
```
<app.minimize.com.seekbar_compat.SeekBarCompat
        ohos:id="$+id:materialSeekBar"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:margin="5vp"
        ohos:max_height="300vp"
        app:progressColor="#AFF123"
        app:thumbColor="#FF4444"/>
```

#### 在java代码中
```
        SeekBarCompat seekBarCompat = (SeekBarCompat) findComponentById(ResourceTable.Id_materialSeekBar);
        seekBarCompat.setThumbColor(Color.MAGENTA.getValue());
        seekBarCompat.setProgressBackgroundColor(Color.TRANSPARENT.getValue());
        seekBarCompat.setProgressColor(Color.RED);
        seekBarCompat.setProgressValue(30);
        seekBarCompat.setThumbAlpha(0);
```

#### 版本迭代
 - v1.0.0
	
	>  	SeekBarCompat是一个Slider的封装库



#### 版权和许可信息

- Apache License




