package app.minimize.com.seekbarcompat;

import app.minimize.com.seekbar_compat.SeekBarCompat;
import app.minimize.com.seekbarcompat.MainAbility;
import app.minimize.com.utils.EventHelper;
import com.yanzhenjie.permission.seekbarcompat.ResourceTable;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ExampleOhosTest {

    private MainAbility mainAbility;

    @Before
    public void before(){
        mainAbility = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(mainAbility,5);
    }

    @After
    public void tearDown(){
        EventHelper.clearAbilities();
    }

    @Test
    public void testSeekBarCompat1(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SeekBarCompat seekBarCompat = (SeekBarCompat) mainAbility.findComponentById(ResourceTable.Id_materialSeekBar);
        EventHelper.inputSwipe(mainAbility,seekBarCompat,0,10,200,10,2000);
        Assert.assertTrue("It's true",seekBarCompat.getProgress()>0);
        int color = Color.getIntColor("#EEEEEE");
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        iAbilityDelegator.runOnUIThreadSync(()->seekBarCompat.setProgressColor(color));
        Assert.assertEquals("It's equals",color,seekBarCompat.getProgressColor().getValue());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSeekBarCompat2(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SeekBarCompat seekBarCompat = (SeekBarCompat) mainAbility.findComponentById(ResourceTable.Id_materialSeekBarTwo);
        Assert.assertTrue("It's true",seekBarCompat.getProgress()==50);
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        iAbilityDelegator.runOnUIThreadSync(()->seekBarCompat.setThumbColor(Color.BLACK.getValue()));
        StateElement stateElement = (StateElement) seekBarCompat.getThumbElement();
        ShapeElement shapeElement = (ShapeElement) stateElement.getStateElement(0);
        assertEquals("It's right",Color.BLACK.getValue(),shapeElement.getRgbColors()[0].asArgbInt());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSeekBarCompat3(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SeekBarCompat seekBarCompat = (SeekBarCompat) mainAbility.findComponentById(ResourceTable.Id_seekBarCompat3);
        EventHelper.inputSwipe(mainAbility,seekBarCompat,0,10,200,10,2000);
        Assert.assertTrue("It's true",seekBarCompat.getProgress()>0);
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        iAbilityDelegator.runOnUIThreadSync(()->seekBarCompat.setProgressBackgroundColor(Color.BLUE.getValue()));
        StateElement stateElement = (StateElement) seekBarCompat.getBackgroundInstructElement();
        ShapeElement shapeElement = (ShapeElement) stateElement.getStateElement(0);
        assertEquals("It's right",Color.BLUE.getValue(),shapeElement.getRgbColors()[0].asArgbInt());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSeekBarCompat4(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int alpha = 200;
        SeekBarCompat seekBarCompat = (SeekBarCompat) mainAbility.findComponentById(ResourceTable.Id_seekBarCompat4);
        EventHelper.inputSwipe(mainAbility,seekBarCompat,0,10,200,10,2000);
        Assert.assertTrue("It's true",seekBarCompat.getProgress()>0);
        seekBarCompat.setThumbAlpha(alpha);
        assertEquals("It's right",alpha,seekBarCompat.getThumbElement().getCurrentElement().getAlpha());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSeekBarCompat5(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SeekBarCompat seekBarCompat = (SeekBarCompat) mainAbility.findComponentById(ResourceTable.Id_seekBarCompat5);
        EventHelper.inputSwipe(mainAbility,seekBarCompat,0,10,200,10,2000);
        Assert.assertTrue("It's true",seekBarCompat.getProgress()>0);
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        iAbilityDelegator.runOnUIThreadSync(()->seekBarCompat.setProgressColor(Color.RED.getValue()));
        Assert.assertEquals("It's equals",Color.RED,seekBarCompat.getProgressColor());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}