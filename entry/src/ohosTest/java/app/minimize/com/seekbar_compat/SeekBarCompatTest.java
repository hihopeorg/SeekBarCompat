package app.minimize.com.seekbar_compat;

import app.minimize.com.seekbar_compat.SeekBarCompat;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.ComponentState;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SeekBarCompatTest {

    @Test
    public void setThumbColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SeekBarCompat seekBarCompat = new SeekBarCompat(context);
        seekBarCompat.setThumbColor(Color.BLACK.getValue());
        StateElement stateElement = (StateElement) seekBarCompat.getThumbElement();
        ShapeElement shapeElement = (ShapeElement) stateElement.getStateElement(0);
        assertEquals("It's right",Color.BLACK.getValue(),shapeElement.getRgbColors()[0].asArgbInt());
    }

    @Test
    public void setProgressColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SeekBarCompat seekBarCompat = new SeekBarCompat(context);
        seekBarCompat.setProgressColor(Color.BLACK.getValue());
        assertEquals("It's right",Color.BLACK.getValue(),seekBarCompat.getProgressColor().getValue());
    }

    @Test
    public void setProgressBackgroundColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SeekBarCompat seekBarCompat = new SeekBarCompat(context);
        seekBarCompat.setProgressBackgroundColor(Color.BLACK.getValue());
        StateElement stateElement = (StateElement) seekBarCompat.getBackgroundInstructElement();
        ShapeElement shapeElement = (ShapeElement) stateElement.getStateElement(0);
        assertEquals("It's right",Color.BLACK.getValue(),shapeElement.getRgbColors()[0].asArgbInt());
    }

    @Test
    public void setThumbElement() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SeekBarCompat seekBarCompat = new SeekBarCompat(context);
        ShapeElement shapeElement1 = new ShapeElement();
        shapeElement1.setShape(ShapeElement.OVAL);
        shapeElement1.setRgbColor(RgbPalette.BLUE);
        ShapeElement shapeElement2 = new ShapeElement();
        shapeElement2.setShape(ShapeElement.OVAL);
        shapeElement2.setRgbColor(RgbPalette.YELLOW);
        StateElement mColorStateListThumb = new StateElement();
        mColorStateListThumb.addState(new int[]{ComponentState.COMPONENT_STATE_DISABLED}, shapeElement1);
        mColorStateListThumb.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, shapeElement2);
        seekBarCompat.setThumbElement(mColorStateListThumb);
        assertEquals("It's right",mColorStateListThumb,seekBarCompat.getThumbElement());
    }

    @Test
    public void setThumbAlpha() {
        int alpha = 200;
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        SeekBarCompat seekBarCompat = new SeekBarCompat(context);
        seekBarCompat.setThumbAlpha(alpha);
        assertEquals("It's right",alpha,seekBarCompat.getThumbElement().getCurrentElement().getAlpha());
    }
}