package app.minimize.com.utils;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.TextTool;
import ohos.app.Context;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AttrUtils {

    /**
     * just for testing method parameters, similar with ohos.agp.components.Attr$AttrType
     */
    public enum AttrType {
        NONE,
        STRING,
        INT,
        LONG,
        FLOAT,
        BOOLEAN,
        DIMENSION,
        ELEMENT,
        COLOR
    }

    public static Attr createAttr(String name, String stringValue, Object value, AttrType attrType, Context context) {
        AttrType type = attrType == null ? AttrType.NONE : attrType;
        // abstract method getType() and inner class ohos.agp.components.Attr$AttrType are hidden,
        // and unluckily AttrType is in the Method signature (return type), so we use dynamic proxy to
        // create Attr instances
        InvocationHandler invocationHandler = new InvocationHandler() {
            @Override
            public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                switch (method.getName()) {
                    case "getName":
                        return name;
                    case "getStringValue":
                        return stringValue;
                    case "getIntegerValue":
                        if (type == AttrType.INT) {
                            return value;
                        }
                        break;
                    case "getBoolValue":
                        if (type == AttrType.BOOLEAN) {
                            return value;
                        }
                        break;
                    case "getFloatValue":
                        if (type == AttrType.FLOAT) {
                            return value;
                        }
                        break;
                    case "getLongValue":
                        if (type == AttrType.LONG) {
                            return value;
                        }
                        break;
                    case "getElement":
                        if (type == AttrType.ELEMENT) {
                            return value;
                        }
                        break;
                    case "getDimensionValue":
                        if (type == AttrType.DIMENSION) {
                            return value;
                        }
                        break;
                    case "getColorValue":
                        if (type == AttrType.COLOR) {
                            return value;
                        }
                        break;
                    case "getContext":
                        return context;
                    case "getType":
                        Class clz = Class.forName("ohos.agp.components.Attr$AttrType");
                        Object[] enumConstants = clz.getEnumConstants();
                        for (Object obj : enumConstants) {
                            if (type.toString().equals(obj.toString())) {
                                return obj;
                            }
                        }
                        break;
                }
                return null;
            }
        };
        return (Attr) Proxy.newProxyInstance(Attr.class.getClassLoader(), new Class[]{Attr.class}, invocationHandler);
    }

    public static AttrSet createAttrSet(List<Attr> attrs) {
        return new AttrSet() {
            List<Attr> attrList = new ArrayList<>();

            {
                attrList.addAll(attrs);
            }

            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return attrList.size();
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                if (i >= 0 && i < attrList.size()) {
                    return Optional.of(attrList.get(i));
                }
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                for (Attr attr : attrList) {
                    if (attr != null && TextTool.isEqual(s, attr.getName())) {
                        return Optional.of(attr);
                    }
                }
                return Optional.empty();
            }
        };
    }
}
