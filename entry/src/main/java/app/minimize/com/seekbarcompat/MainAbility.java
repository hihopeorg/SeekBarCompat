
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package app.minimize.com.seekbarcompat;

import app.minimize.com.seekbar_compat.SeekBarCompat;
import app.minimize.com.seekbarcompat.slice.MainAbilitySlice;
import com.yanzhenjie.permission.seekbarcompat.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Slider;
import ohos.agp.utils.Color;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        super.setUIContent(ResourceTable.Layout_ability_main);

        SeekBarCompat seekBarCompat = (SeekBarCompat) findComponentById(ResourceTable.Id_materialSeekBar);
        seekBarCompat.setThumbColor(Color.MAGENTA.getValue());
        seekBarCompat.setProgressBackgroundColor(Color.TRANSPARENT.getValue());
        seekBarCompat.setProgressColor(Color.RED);
        seekBarCompat.setProgressValue(30);
        seekBarCompat.setThumbAlpha(0);

        SeekBarCompat seekBarCompatTwo = (SeekBarCompat) findComponentById(ResourceTable.Id_materialSeekBarTwo);
        seekBarCompatTwo.setProgressBackgroundColor(Color.RED.getValue());
        seekBarCompatTwo.setProgressValue(50);
        seekBarCompatTwo.setEnabled(false);

        Slider normalSeekbar = (Slider) findComponentById(ResourceTable.Id_normalSeekbar);

    }
}
