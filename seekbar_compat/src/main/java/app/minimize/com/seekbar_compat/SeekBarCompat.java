package app.minimize.com.seekbar_compat;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentState;
import ohos.agp.components.Slider;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;


public class SeekBarCompat extends Slider implements Component.TouchEventListener {

    private static final String TAG = "SeekBarCompat";

    private int mActualBackgroundColor;

    /***
     * Thumb and Progress colors
     */
    private int mThumbColor;
    private int mProgressColor;
    private int mProgressBackgroundColor;

    /***
     * Thumb drawable
     */
    private Element mThumb;

    /***
     * States for Lollipop ColorStateList
     */
    private int[][] states = {{ComponentState.COMPONENT_STATE_EMPTY}, {ComponentState.COMPONENT_STATE_PRESSED}
            , {ComponentState.COMPONENT_STATE_DISABLED}};

    /***
     * Default colors to be black for Thumb ColorStateList
     */
    private int[] colorsThumb = {Color.BLACK.getValue(), Color.BLACK.getValue(), Color.LTGRAY.getValue(), Color.BLACK.getValue()};

    /***
     * Default colors to be black for Progress ColorStateList
     */
    private int[] colorsProgress = {Color.BLACK.getValue(), Color.BLACK.getValue(), Color.LTGRAY.getValue(), Color.BLACK.getValue()};

    /***
     * Default colors to be black for Progress ColorStateList
     */
    private int[] colorsProgressBackground = {Color.BLACK.getValue(), Color.BLACK.getValue(), Color.LTGRAY.getValue(), Color.BLACK.getValue()};

    /***
     * ColorStateList objects
     */
    private StateElement mColorStateListThumb;
    private StateElement mColorStateListProgress;
    private StateElement mColorStateListProgressBackground;

    /***
     * Used for APIs below 21 to determine height of the seekBar as well as the new thumb drawable
     */
    private int mOriginalThumbHeight;
    private int mThumbAlpha = 255;

    public SeekBarCompat(Context context) {
        this(context, null);
    }

    public SeekBarCompat(Context context, AttrSet attrSet) {
        this(context, attrSet, "0");
    }

    public SeekBarCompat(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context, attrSet);
    }

    private void init(Context context, AttrSet attrs) {
        if (attrs != null && attrs.getAttr(SeekBarCompatAttr.SeekBarCompat_thumbColor).isPresent()) {
            mThumbColor = attrs.getAttr(SeekBarCompatAttr.SeekBarCompat_thumbColor).get().getColorValue().getValue();
        } else {
            mThumbColor = getPrimaryColorFromSelectedTheme(context);
        }
        if (attrs != null && attrs.getAttr(SeekBarCompatAttr.SeekBarCompat_progressColor).isPresent()) {
            mProgressColor = attrs.getAttr(SeekBarCompatAttr.SeekBarCompat_progressColor).get().getColorValue().getValue();
        } else {
            mProgressColor = getPrimaryColorFromSelectedTheme(context);
        }
        if (attrs != null && attrs.getAttr(SeekBarCompatAttr.SeekBarCompat_progressBackgroundColor).isPresent()) {
            mProgressBackgroundColor = attrs.getAttr(SeekBarCompatAttr.SeekBarCompat_progressBackgroundColor).get().getColorValue().getValue();
        } else {
            mProgressBackgroundColor = getPrimaryColorFromSelectedTheme(context);
        }
        float alpha = 0;
        if (attrs != null && attrs.getAttr(SeekBarCompatAttr.SeekBarCompat_thumbAlpha).isPresent()) {
            alpha = attrs.getAttr(SeekBarCompatAttr.SeekBarCompat_thumbAlpha).get().getFloatValue();
        } else {
            alpha = 1f;
        }
        mThumbAlpha = (int) (alpha * 255);
        mActualBackgroundColor = Color.TRANSPARENT.getValue();
//        splitTrack = false;
        setupThumbColorLollipop();
        setupProgressColorLollipop();
        setupProgressBackgroundLollipop();
        mColorStateListThumb.getCurrentElement().setAlpha(mThumbAlpha);

    }

    /***
     * Updates the thumbColor dynamically
     *
     * @param thumbColor Color representing thumb drawable
     */
    public void setThumbColor(int thumbColor) {
        mThumbColor = thumbColor;
        setupThumbColorLollipop();
        invalidate();
        postLayout();
    }

    /***
     * Method called for APIs 21 and above to setup thumb Color
     */
    private void setupThumbColorLollipop() {
        colorsThumb[0] = mThumbColor;
        colorsThumb[1] = mThumbColor;
        colorsThumb[2] = Color.LTGRAY.getValue();
        ShapeElement shapeElement1 = new ShapeElement();
        shapeElement1.setShape(ShapeElement.OVAL);
        shapeElement1.setRgbColor(RgbColor.fromArgbInt(colorsThumb[0]));
        ShapeElement shapeElement2 = new ShapeElement();
        shapeElement2.setShape(ShapeElement.OVAL);
        shapeElement2.setRgbColor(RgbColor.fromArgbInt(colorsThumb[1]));
        ShapeElement shapeElement3 = new ShapeElement();
        shapeElement3.setShape(ShapeElement.OVAL);
        shapeElement3.setRgbColor(RgbColor.fromArgbInt(colorsThumb[2]));
        mColorStateListThumb = new StateElement();
        mColorStateListThumb.addState(states[0], shapeElement1);
        mColorStateListThumb.addState(states[1], shapeElement2);
        mColorStateListThumb.addState(states[2], shapeElement3);
        setThumbElement(mColorStateListThumb);
    }

    /***
     * Updates the progressColor dynamically
     *
     * @param progressColor Color representing progress drawable
     */
    public void setProgressColor(int progressColor) {
        mProgressColor = progressColor;
        setupProgressColorLollipop();
        invalidate();
        postLayout();
    }

    private void setupProgressColorLollipop() {
        setProgressColor(new Color(mProgressColor));
    }

    public void setProgressBackgroundColor(int progressBackgroundColor) {
        mProgressBackgroundColor = progressBackgroundColor;
        setupProgressBackgroundLollipop();
        invalidate();
        postLayout();
    }

    private void setupProgressBackgroundLollipop() {
        colorsProgressBackground[0] = mProgressBackgroundColor;
        colorsProgressBackground[1] = mProgressBackgroundColor;
        ShapeElement shapeElement1 = new ShapeElement();
        shapeElement1.setAlpha(100);
        shapeElement1.setRgbColor(RgbColor.fromArgbInt(colorsProgressBackground[0]));
        ShapeElement shapeElement2 = new ShapeElement();
        shapeElement2.setAlpha(100);
        shapeElement2.setRgbColor(RgbColor.fromArgbInt(colorsProgressBackground[1]));
        mColorStateListProgressBackground = new StateElement();
        mColorStateListProgressBackground.addState(states[0], shapeElement1);
        mColorStateListProgressBackground.addState(states[1], shapeElement2);
        setProgressBackgroundElement(mColorStateListProgressBackground);
    }



    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        return false;
    }

    @Override
    public void setThumbElement(Element element) {
        super.setThumbElement(element);
        mThumb = element;
        setLayoutConfig(getLayoutConfig());
    }

    public void setThumbAlpha(int alpha){
        mThumbAlpha = alpha;
        mColorStateListThumb.getCurrentElement().setAlpha(mThumbAlpha);
    }

    /***
     * Gets the Primary Color from theme
     *
     * @param context Context Object
     * @return Primary Color
     */
    public static int getPrimaryColorFromSelectedTheme(Context context){
        return Color.LTGRAY.getValue();
    }


}
